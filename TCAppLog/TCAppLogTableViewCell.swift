//
//  TCAppLogTableViewCell.swift
//  BNK48MemberApp
//
//  Created by Peerasak Unsakon on 1/2/2564 BE.
//  Copyright © 2564 BE Jeerapon. All rights reserved.
//

import UIKit

class TCAppLogTableViewCell: UITableViewCell {

    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var featureLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timeStampLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func configure(logItem: TCAppLogDescriptor) {
        self.typeLabel.text = logItem.type.toTitle
        self.typeLabel.backgroundColor = logItem.type.toColor
        self.featureLabel.text = logItem.feature
        self.messageLabel.text = logItem.message
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "th_TH")
        formatter.dateFormat = "d MMM yy HH:mm:ss น."
        
        self.timeStampLabel.text = formatter.string(from: logItem.timestamp)
    }
    
}

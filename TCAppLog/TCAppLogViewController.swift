//
//  TCAppLogViewController.swift
//  BNK48MemberApp
//
//  Created by Peerasak Unsakon on 1/2/2564 BE.
//  Copyright © 2564 BE Jeerapon. All rights reserved.
//

import UIKit

public class TCAppLogViewController: UIViewController {

    @IBOutlet weak var statusTitle: UILabel!
    @IBOutlet weak var uploadButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    public var selectedFeature: String?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        let podBundle = Bundle(for: TCAppLog.self)
        if let bundleURL = podBundle.url(forResource: "TCAppLog", withExtension: "bundle") {
            if let bundle = Bundle(url: bundleURL) {
                self.tableView.register(UINib.init(nibName: "TCAppLogTableViewCell", bundle: bundle), forCellReuseIdentifier: "TCAppLogTableViewCell")
                self.tableView.estimatedRowHeight = 100
                self.tableView.dataSource = self
                self.tableView.delegate = self
            }
        }else {
            self.tableView.register(UINib.init(nibName: "TCAppLogTableViewCell", bundle: nil), forCellReuseIdentifier: "TCAppLogTableViewCell")
            self.tableView.estimatedRowHeight = 100
            self.tableView.dataSource = self
            self.tableView.delegate = self
        }
        
    }

    @IBAction func uploadButtonTapped(_ sender: Any) {
        self.pushLogs()
    }
    
    @IBAction func clearButtonTapped(_ sender: Any) {
        TCAppLog.clearLogs()
        self.tableView.reloadData()
        self.statusTitle.text = "Application Logs"
    }
    
    private func pushLogs() {
        guard let logs = TCAppLog.getLogs(feature: self.selectedFeature) as? [TCLogItem] else {
            self.statusTitle.text = "Error"
            return
        }
        
        self.statusTitle.text = "Uploading ..."
        self.uploadButton.isEnabled = false
        
        TCAppLogService().push(logs: logs) { [weak self] (sucess) in
            DispatchQueue.main.async {
                self?.statusTitle.text = "Upload Completed"
                self?.uploadButton.isEnabled = true
            }
        } reject: { [weak self] (error) in
            DispatchQueue.main.async {
                self?.statusTitle.text = "Upload Failed"
                self?.uploadButton.isEnabled = true
            }
        }
    }
}

extension TCAppLogViewController: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TCAppLog.numberOfRows(feature: self.selectedFeature)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "TCAppLogTableViewCell", for: indexPath) as? TCAppLogTableViewCell else {
            fatalError("CELL NOT FOUND")
        }
        cell.configure(logItem: TCAppLog.logItem(at: indexPath, feature: self.selectedFeature))
        return cell
        
    }
    
}

extension TCAppLogViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


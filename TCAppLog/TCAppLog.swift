//
//  TCAppLog.swift
//  BNK48MemberApp
//
//  Created by Peerasak Unsakon on 1/2/2564 BE.
//  Copyright © 2564 BE Jeerapon. All rights reserved.
//

import Foundation
import UIKit

public protocol TCAppLogDescriptor {
    var type: TCAppLog.LogType { get }
    var feature: String { get }
    var timestamp: Date { get }
    var message: String { get }
}

public struct TCLogItem: TCAppLogDescriptor, Codable {
    public var type: TCAppLog.LogType
    public var feature: String = "global"
    public var timestamp: Date = Date()
    public var message: String
}

public final class TCAppLogService {
    
    func push(logs: [TCLogItem], completion: @escaping (Bool) -> Void, reject: @escaping (Error) -> Void) {
        let url = URL(string: "https://script.google.com/macros/s/\(TCAppLog.shared.serviceId)/exec")!
        var request = URLRequest(url: url)
        request.setValue(
            "application/json",
            forHTTPHeaderField: "Content-Type"
        )

        DispatchQueue.global(qos: .userInitiated).async {
            do {
                let jsonData = try JSONEncoder().encode(logs)
                request.httpMethod = "POST"
                request.httpBody = jsonData
            } catch {
                TCAppLog.errorLog(text: error.localizedDescription)
            }
            
            let session = URLSession.shared
            let task = session.dataTask(with: request) { (data, response, error) in

                if let error = error {
                    reject(error)
                } else if let _ = data {
                    completion(true)
                } else {
                    completion(false)
                }
            }
            
            task.resume()
        }
    }
}

public final class TCAppLog {
    
    public enum LogType: String, Codable {
        case debug = "debug"
        case info = "info"
        case warning = "warning"
        case error = "error"
        
        var toString: String {
            switch self {
            case .debug:
                return "DBL"
            case .error:
                return "ERL"
            case .info:
                return "IFL"
            case .warning:
                return "WNL"
            }
        }
        
        var toTitle: String {
            switch self {
            case .debug:
                return "DEBUG"
            case .error:
                return "ERROR"
            case .info:
                return "INFO"
            case .warning:
                return "WARNING"
            }
        }
        
        var toColor: UIColor {
            switch self {
            case .debug:
                return .gray
            case .error:
                return .systemRed
            case .info:
                return .blue
            case .warning:
                return .systemOrange
            }
        }
    }

    
    public static let shared: TCAppLog = {
        let instance = TCAppLog()
        return instance
    }()
    
    public var serviceId: String = ""
    
    private var store: [TCAppLogDescriptor] = []
    
    private func saveLog(logItem: TCAppLogDescriptor) {
        self.store.append(logItem)
    }
    
    private func log(type: LogType, feature: String, msg: String, save: Bool) {
        debugPrint("[APPLOG:\(type.toString) - \(Date())] - \(msg)")
        if save {
            let log = TCLogItem(type: type, feature: feature, timestamp: Date(), message: msg)
            self.saveLog(logItem: log)
        }
    }
    
    public static func numberOfRows(feature: String? = nil) -> Int {
        guard
            let feature = feature else {
            return TCAppLog.shared.store.count
        }
        return TCAppLog.shared.store.filter({ (item) -> Bool in
            item.feature == feature
        }).count
    }
    
    public static func getLogs(feature: String? = nil) -> [TCAppLogDescriptor] {
        guard
            let feature = feature else {
            return TCAppLog.shared.store
        }
        return TCAppLog.shared.store.filter({ (item) -> Bool in
            item.feature == feature
        })
    }
    
    public static func clearLogs() {
        TCAppLog.shared.store.removeAll()
    }
    
    public static func configure(key: String) {
        TCAppLog.shared.serviceId = key
    }
    
    public static func logItem(at indexPath: IndexPath, feature: String? = nil) -> TCAppLogDescriptor {
        guard
            let feature = feature else {
            return TCAppLog.shared.store[indexPath.row]
        }
        return TCAppLog.shared.store.filter({ (item) -> Bool in
            item.feature == feature
        })[indexPath.row]
    }
    
    public static func debugLog(text: String, feature: String = "global", save: Bool = false) {
        TCAppLog.shared.log(type: .debug, feature: feature, msg: text, save: save)
    }
    
    public static func infoLog(text: String, feature: String = "global", save: Bool = false) {
        TCAppLog.shared.log(type: .info, feature: feature, msg: text, save: save)
    }
    
    public static func warningLog(text: String, feature: String = "global", save: Bool = false) {
        TCAppLog.shared.log(type: .warning, feature: feature, msg: text, save: save)
    }
    
    public static func errorLog(text: String, feature: String = "global", save: Bool = false) {
        TCAppLog.shared.log(type: .error, feature: feature, msg: text, save: save)
    }
    
}

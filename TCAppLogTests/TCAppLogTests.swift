//
//  TCAppLogTests.swift
//  TCAppLogTests
//
//  Created by Peerasak Unsakon on 8/2/2564 BE.
//

import XCTest
@testable import TCAppLog

class TCAppLogTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testAddLog() throws {
        TCAppLog.debugLog(text: "debug message", feature: "meetingyou", save: true)
        TCAppLog.warningLog(text: "warning message", feature: "meetingyou", save: true)
        TCAppLog.debugLog(text: "just debug")
        TCAppLog.warningLog(text: "warning message", feature: "global", save: true)
        TCAppLog.errorLog(text: "error message", feature: "global", save: true)
        XCTAssertEqual(TCAppLog.getLogs().count, 4)
    }
    
    func testGetFeatureLog() throws {
        TCAppLog.debugLog(text: "debug message", feature: "meetingyou", save: true)
        TCAppLog.warningLog(text: "warning message", feature: "meetingyou", save: true)
        TCAppLog.debugLog(text: "just debug")
        TCAppLog.warningLog(text: "warning message", feature: "global", save: true)
        TCAppLog.errorLog(text: "error message", feature: "global", save: true)
        XCTAssertEqual(TCAppLog.getLogs(feature: "meetingyou").count, 2)
    }
    
    func testClearLog() throws {
        TCAppLog.clearLogs()
        XCTAssertEqual(TCAppLog.getLogs().count, 0)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
